package prj4part1;

public class SlidingWindow {

	private byte[][] slidingWindow;
	public int windowLength;
	
	public SlidingWindow(int length) {
		this.windowLength = length;
		slidingWindow = new byte[length][];
	}
	
	public void addBytes(byte[] bytes) {
		for (int i = 0; i < windowLength; i++) {
			if (slidingWindow[i] == null) {
				slidingWindow[i] = bytes;
				//return true;
				return;
			}
		}
		//return false;
	}
	
	public void addBytes(byte[] bytes, int index) {
		slidingWindow[index] = bytes;
	}
	
	public byte[] getBytes(int index) {
		return slidingWindow[index];
	}
	
	public void removeBytes(int index) {
		slidingWindow[index] = null;
	}
	
	public String getBytesSeqNum(int index) {
		String tag = new String(slidingWindow[index]);
		int index1 = tag.indexOf("#");
		int index2 = tag.indexOf("#", index1 + 1);
		String seqNum = tag.substring(index1 + 1, tag.indexOf(","));
		return seqNum;
	}
	
	public void shiftWindowRightOne() {
		for (int i = 0; i < windowLength; i++) {
			if (i == windowLength - 1) {
				slidingWindow[i] = null;
			} else {
				slidingWindow[i] = slidingWindow[i+1];
			}
		}
	}
	
	public boolean gotACK(int index) {
		if(index == 0) {
			shiftWindowRightOne();
			return true;
		}
		
		return false;
	}
	
	public String toString() {
		String temp = "";
		for (int i = 0; i < windowLength; i++) {
			if (slidingWindow[i] == null) {
				temp += "Spot: " + (i+1) + "\nnull\n"; 
			} else {
				temp += "Spot: " + (i+1) + "\n" + new String(slidingWindow[i]) + "\n";
			}
		}
		return temp;
	}
}
