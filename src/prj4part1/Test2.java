package prj4part1;

import java.awt.List;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class Test2 {

	public static int seqNum;
	public static String fileName;
	public static long fileOffset;
	public static long fileSizeRemaining;
	public static boolean fileSizeSet;
	public static long fileSize;
	public static int totalTagSize;
	public static boolean fileRead;
	
	public static void main(String[] args) throws Exception {
		fileName = "src/testFiles/testFile2";
		seqNum = 1;
		fileSizeSet = false;
		fileRead = false;
		fileSizeRemaining = 1;
		totalTagSize = 0;
		
		
		ArrayList<byte[]> testBytes = new ArrayList<byte[]>();
		boolean bo = false;
		while (!fileRead) {
			byte[] temp = readFileByte();
//			if (temp == null) {
//				bo = true;
//				System.out.println("here");
//				break;
//			}
			testBytes.add(temp);
			System.out.println("here2");
		}
		
		long size = 0;
		for(byte[] b : testBytes) {
			System.out.println(b.length);
			size += b.length;
			System.out.println(new String(b));
			System.out.println("-----------");
		}
		System.out.println("-----------");
		System.out.println("Total bytes: " + size);
		System.out.println("-----------");
		System.out.println("File size: " + fileSize);
		System.out.println("-----------");
		System.out.println("Total tag size: " + totalTagSize);
		
		
		
		int[] t = new int[]{1,2,3,4,5};
		t[0] = t[1];
		t[1] = t[2];
		t[2] = t[3];
		t[3] = t[4];
		t[4] = -1;
		
		System.out.println(t[0]);
		System.out.println(t[1]);
		System.out.println(t[2]);
		System.out.println(t[3]);
		System.out.println(t[4]);
	}
	
	public static byte[] readFileByte() throws Exception {
		if (fileRead) {
			return null;
		}
		
		//System.out.println("file remaining: " + fileSizeRemaining);
		RandomAccessFile file = new RandomAccessFile(fileName, "r");
		if (!fileSizeSet) {
			fileSizeRemaining = file.length();
			fileSize = file.length();
			fileSizeSet = true;
		}
		file.seek(fileOffset);
		
		String seqNumTag = "#" + seqNum + "," + fileOffset + "#";
        byte[] tagBytes = seqNumTag.getBytes();
        int tagSize = tagBytes.length;
        totalTagSize += tagSize;
        
        int bufferSize = 0;
        if ((fileSizeRemaining / (1024 - tagSize)) >= 1) {
        	bufferSize = 1024 - tagSize;
        } else {
        	bufferSize = (int) fileSizeRemaining;
        }
        
        byte[] fileBytes = new byte[bufferSize];
		file.read(fileBytes, 0, fileBytes.length);
		file.close();
		
		fileOffset += bufferSize;
		fileSizeRemaining -= bufferSize;
		seqNum++;
		
		if (fileSizeRemaining == 0) {
			fileRead = true;
		}
		
		return combineByteArrays(tagBytes, fileBytes);
	}
	
	public static byte[] combineByteArrays(byte[] array1, byte[] array2) {
		int len1 = array1.length;
		int len2 = array2.length;
		byte[] combined = new byte[len1 + len2];
		System.arraycopy(array1, 0, combined, 0, len1);
		System.arraycopy(array2, 0, combined, len1, len2);
		return combined;
	}
}
