package prj4part1;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Server {
	
	private DatagramSocket serverSocket;
	private int seqNum;
	private String fileName;
	private long fileOffset;
	private long fileSizeRemaining;
	private boolean fileSizeSet;
	private long fileSize;
	private int totalTagSize;
	private boolean fileRead;
	private SlidingWindow sw;
	private boolean transferComplete;
	
	public Server(int pPortNum) throws Exception {
		serverSocket = new DatagramSocket(pPortNum);
		seqNum = 1;
		fileSizeSet = false;
		fileRead = false;
		fileSizeRemaining = 1;
		transferComplete = false;
		sw = new SlidingWindow(5);
	}
	
	public void fillSlidingWindow() throws Exception {
		sw.addBytes(readFileByte(), 0);
		sw.addBytes(readFileByte(), 1);
		sw.addBytes(readFileByte(), 2);
		sw.addBytes(readFileByte(), 3);
		sw.addBytes(readFileByte(), 4);
	}
	
	public void sendFileBytes(InetAddress clientIP, int clientPort) throws Exception {
//		while (!transferComplete) {
//			for (int i = 0; i < sw.windowLength; i++) {
//				byte[] data = sw.getBytes(i);
//				if (data != null) {
//					DatagramPacket dataPacket = new DatagramPacket(data, data.length, clientIP, clientPort);
//					serverSocket.send(dataPacket);
//					System.out.println("SERVER: Sent data packet with sequence number " + sw.getBytesSeqNum(i) + ".");
//				}
//			}
//			transferComplete = true;
//			//receivePacket();
//			
//		}
		int count = 0;
		while(count < 37) {
			byte[] data = readFileByte();
			DatagramPacket dataPacket = new DatagramPacket(data, data.length, clientIP, clientPort);
			serverSocket.send(dataPacket);
		}
	}
	
	public void sendAck() {
		
	}
	
	private void receiveFileName() {
		byte[] receiveFileName = new byte[1024];
		DatagramPacket fileNamePacket = new DatagramPacket(receiveFileName, receiveFileName.length);
		try {
			serverSocket.receive(fileNamePacket);
			fileName = new String(receiveFileName).trim();
			System.out.println("File requested: " + fileName);
			InetAddress clientIP = fileNamePacket.getAddress();
			int clientPort = fileNamePacket.getPort();
//			String ackString = "#FILE_NAME_ACK#";
//	        byte[] ack = ackString.getBytes();
//	        DatagramPacket ackPacket = 
//					new DatagramPacket(ack, ack.length, clietIPAddress, clientPort);
//			serverSocket.send(ackPacket);
			//fillSlidingWindow();
			//System.out.println(sw.toString());
			sendFileBytes(clientIP, clientPort);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void receivePacket() {
		byte[] receiveData = new byte[1024];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		try {
			serverSocket.receive(receivePacket);
			InetAddress clietIPAddress = receivePacket.getAddress();
			int clientPort = receivePacket.getPort();
			String clientMessage = new String(receiveData);
			int index1 = clientMessage.indexOf("#");
			int index2 = clientMessage.indexOf("#", index1 + 1);
			//Get an identifier string to see what type of message it is
			String identifier = clientMessage.substring(index1 + 1, index2);
			if (identifier.substring(0,4).equals("ACK:")) {
				String seqNum = identifier.substring(4, index2);
				if(sw.gotACK(Integer.parseInt(seqNum))) {
					sw.addBytes(readFileByte());
				}
				System.out.println("SERVER: Received ACK for packet with sequence number " + seqNum + ".");
			}
			
			//String ackString = "#FILE_NAME_ACK#";
	        //byte[] ack = ackString.getBytes();
	        //DatagramPacket ackPacket = 
			//		new DatagramPacket(ack, ack.length, clietIPAddress, clientPort);
			//serverSocket.send(ackPacket);
	        
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private byte[] combineByteArrays(byte[] array1, byte[] array2) {
		int len1 = array1.length;
		int len2 = array2.length;
		byte[] combined = new byte[len1 + len2];
		System.arraycopy(array1, 0, combined, 0, len1);
		System.arraycopy(array2, 0, combined, len1, len2);
		return combined;
	}
	
	public byte[] readFileByte() throws Exception {
		if (fileRead) {
			return null;
		}
		
		//System.out.println("file remaining: " + fileSizeRemaining);
		RandomAccessFile file = new RandomAccessFile(fileName, "r");
		if (!fileSizeSet) {
			fileSizeRemaining = file.length();
			fileSize = file.length();
			fileSizeSet = true;
		}
		file.seek(fileOffset);
		
		String seqNumTag = "#" + seqNum + "," + fileOffset + "#";
        byte[] tagBytes = seqNumTag.getBytes();
        int tagSize = tagBytes.length;
        totalTagSize += tagSize;
        
        int bufferSize = 0;
        if ((fileSizeRemaining / (1024 - tagSize)) >= 1) {
        	bufferSize = 1024 - tagSize;
        } else {
        	bufferSize = (int) fileSizeRemaining;
        }
        
        byte[] fileBytes = new byte[bufferSize];
		file.read(fileBytes, 0, fileBytes.length);
		file.close();
		
		fileOffset += bufferSize;
		fileSizeRemaining -= bufferSize;
		seqNum++;
		
		if (fileSizeRemaining == 0) {
			fileRead = true;
		}
		
		return combineByteArrays(tagBytes, fileBytes);
	}
	
	public static void main(String[] args) throws Exception {
		Server s = new Server(1994);
		s.receiveFileName();
	}
}