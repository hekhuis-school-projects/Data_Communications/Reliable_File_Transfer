package prj4part1;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Client {
	
	private DatagramSocket clientSocket;
	private InetAddress ipAddress;
	private int portNum;
	private String fileName;
	private String savePath;
	private ArrayList<String> received;
	
	public Client(String pIP, int pPortNum) throws Exception {
		clientSocket = new DatagramSocket();
		ipAddress = InetAddress.getByName(pIP);
		this.portNum = pPortNum;
		fileName = "src/saveFolder/saveFilePic.png";
	}
	
	private void requestFile(String fileName) {
		byte[] sendData = fileName.getBytes();
		DatagramPacket sendPacket = new DatagramPacket(sendData,sendData.length,ipAddress,portNum);
		try {
			clientSocket.send(sendPacket);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void receivePacket() {
		byte[] receiveData = new byte[1024];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		try {
			clientSocket.receive(receivePacket);
			String tag = new String(receiveData);
			int index1 = tag.indexOf("#");
			int index2 = tag.indexOf("#", index1 + 1);
			tag = tag.substring(index1, index2 + 1);
			int tagLength = tag.length();
			String offset = tag.substring(tag.indexOf(",") + 1, index2);
			String seqNum = tag.substring(index1 + 1, tag.indexOf(",")); 
			saveToFile(receiveData, tagLength, offset);
			System.out.println("CLIENT: Received packet with sequence number " + seqNum + ".");
			String ackString = "#ACK:" + seqNum + "#";
			byte[] ack = ackString.getBytes();
			DatagramPacket sendPacket = new DatagramPacket(ack, ack.length,ipAddress,portNum);
			System.out.println("CLIENT: Sent ACK for packet with sequence number " + seqNum + ".");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void saveToFile(byte[] bytes, int tagLength, String off) throws Exception {
		//System.out.println("here");
		RandomAccessFile file = new RandomAccessFile(fileName, "rw");
		
		int offset = Integer.parseInt(off);
		byte[] data = new byte[bytes.length - tagLength];
		System.arraycopy(bytes, tagLength, data, 0, data.length);
		file.seek(offset);
		file.write(data);
		file.close();
	}
	
	public static void main(String[] args) throws Exception {
		Client c = new Client("127.0.0.1", 1994);
		c.requestFile("src/testFiles/pic1.png");
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
		c.receivePacket();
	}
}
