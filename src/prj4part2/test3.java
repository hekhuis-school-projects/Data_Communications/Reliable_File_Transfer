package prj4part2;

import java.io.RandomAccessFile;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.zip.CRC32;

public class test3 {

	private DatagramSocket serverSocket;
	private static int seqNum;
	private static String fileName;
	private static long fileOffset;
	private static long fileSizeRemaining;
	private static boolean fileSizeSet;
	private static long fileSize;
	private static int totalTagSize;
	private static boolean fileRead;
	private SlidingWindow sw;
	private static boolean transferComplete;
	private ArrayList<Integer> receivedAcks;
	private static boolean sizeReceived;
	private static final int PACKET_SIZE_AFTER_CHECKSUM = 1016;
	private int amountOfAcks;
	private static boolean fileNameReceived;
	private static boolean windowInitialized;
	private static CRC32 crc32;
	
	public static void main(String[] args) throws Exception {
		seqNum = 1;
		fileSizeSet = false;
		fileRead = false;
		fileSizeRemaining = 1;
		transferComplete = false;
		//initializeSlidingWindow();
		sizeReceived = false;
		fileNameReceived = false;
		windowInitialized = false;
		crc32 = new CRC32();
		fileName = "src/testFiles/testFile2";
		InetAddress ipAddress = InetAddress.getByName("127.0.0.1");
		int portNum = 1994;
		
		
		System.out.println("long bytes: " + Long.BYTES);
		byte[] b1 = readFileByte();
		//byte[] temp = ChecksumUtil.getChecksum(b1);
		//System.out.println(temp.length);
		//System.out.println(ChecksumUtil.bytesToLong(temp));
		//System.out.println(new String(b1));
		//DataPacket dp1 = new DataPacket(b1, ipAddress, portNum);
		//System.out.println("dp1 seq num: " + dp1.getSeqNum());
		//System.out.println(dp1.toString());
		//byte[] b2 = readFileByte();
		//System.out.println(new String(b2));
		//DataPacket dp2 = new DataPacket(b2, ipAddress, portNum);
		//System.out.println("dp2 seq num: " + dp2.getSeqNum());
		//System.out.println(dp2.toString());
		//byte[] b3 = readFileByte();
		//System.out.println(new String(b3));
		//DataPacket dp3 = new DataPacket(b3, ipAddress, portNum);
		//System.out.println("dp3 seq num: " + dp3.getSeqNum());
		//System.out.println(dp3.toString());
		//byte[] b4 = readFileByte();
		//System.out.println(new String(b4));
		//DataPacket dp4 = new DataPacket(b4, ipAddress, portNum);
		//System.out.println("dp4 seq num: " + dp4.getSeqNum());
		//System.out.println(dp4.toString());
		//byte[] b5 = readFileByte();
		//System.out.println(new String(b5));
	}
	
	private static byte[] readFileByte() throws Exception {
		//System.out.println("File read: " + fileRead);
		if (fileRead) {
			return null;
		}
		
		//System.out.println("file remaining: " + fileSizeRemaining);
		//System.out.println(fileName);
		RandomAccessFile file = new RandomAccessFile(fileName, "r");
		if (!fileSizeSet) {
			fileSizeRemaining = file.length();
			fileSize = file.length();
			System.out.println("SERVER: File size is: " + fileSize);
			fileSizeSet = true;
			 
		}
		file.seek(fileOffset);
		
		String seqNumTag = "#DATA:" + seqNum + "," + fileOffset + "#";
        byte[] tagBytes = seqNumTag.getBytes();
        int tagSize = tagBytes.length;
        totalTagSize += tagSize;
        
        int bufferSize = 0;
        if ((fileSizeRemaining / (PACKET_SIZE_AFTER_CHECKSUM - tagSize)) >= 1) {
        	bufferSize = PACKET_SIZE_AFTER_CHECKSUM - tagSize;
        } else {
        	bufferSize = (int) fileSizeRemaining;
        }
        
        byte[] fileBytes = new byte[bufferSize];
		file.read(fileBytes, 0, fileBytes.length);
		file.close();
		
		fileOffset += bufferSize;
		fileSizeRemaining -= bufferSize;
		seqNum++;
		
		if (fileSizeRemaining == 0) {
			fileRead = true;
		}
		byte[] beforeChecksum = combineByteArrays(tagBytes, fileBytes);
		//System.out.println(beforeChecksum.length);
		//System.out.println(new String(beforeChecksum));
		//byte[] checksumBytes = ByteBuffer.allocate(Long.SIZE / Byte.SIZE).putLong(checksum).array();
		//byte[] checksumBytes = ChecksumUtil.getChecksum(beforeChecksum);
		//System.out.println(checksumBytes.length);
		byte[] afterChecksum = ChecksumUtil.addChecksum(beforeChecksum);
		System.out.println(afterChecksum.length);
		System.out.println(new String(afterChecksum));
		return afterChecksum;
	}

	private static byte[] combineByteArrays(byte[] array1, byte[] array2) {
		int len1 = array1.length;
		int len2 = array2.length;
		byte[] combined = new byte[len1 + len2];
		System.arraycopy(array1, 0, combined, 0, len1);
		System.arraycopy(array2, 0, combined, len1, len2);
		return combined;
	}
	
	public static long bytesToLong(byte[] bytes) {
	    ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
	    buffer.put(bytes);
	    buffer.flip();//need flip 
	    return buffer.getLong();
	}
}