package prj4part2;

import java.net.DatagramPacket;
import java.net.InetAddress;

/**********************************************************************
 * The DataPacket class represents a packet of data with boolean that
 * concerns the packets acknowledgement status.
 * @author Kyle Hekhuis
 **********************************************************************/
public class DataPacket {

	/** Represents if packet has been acknowledged or not. */
	private boolean ack;
	/** Contains the packet info needed to send this data packet. */
	private DatagramPacket packet;
	/** Sequence number of this packet. */
	private int seqNum;
	/** Total size of this DataPacket. */
	private int packetSize;
	/** The file data held in the packet. */
	private byte[] packetData;
	
	/********************************************************
	 * Creates a new DataPacket with the passed parameters.
	 * @param data file data held in the packet
	 * @param ip IP of this packet's destination
	 * @param port port number of this packet's destination
	 ********************************************************/
	public DataPacket(byte[] data, InetAddress ip, int port) {
		packet = new DatagramPacket(data, data.length, ip, port);
		ack = false;
		setVariables(data);
	}
	
	/******************************************************
	 * Returns the DatagramPacket held in this DataPacket.
	 * @return the DatagramPacket held in this DataPacket
	 ******************************************************/
	public DatagramPacket getUDPPacket() {
		return packet;
	}
	
	/**********************************************************
	 * Returns this packets acknowledgement status.
	 * @return boolean for this packets acknowledgement status
	 *********************************************************/
	public boolean getAck() {
		return ack;
	}
	
	/**********************************************************
	 * Sets the acknowledgement status for this DataPacket.
	 * @param ack acknowledgement status to set for DataPacket
	 **********************************************************/
	public void setAck(boolean ack) {
		this.ack = ack;
	}
	
	/**************************************************
	 * Returns the sequence number of this DataPacket.
	 * @return the sequence number of this DataPacket
	 **************************************************/
	public int getSeqNum() {
		return seqNum;
	}
	
	/****************************************
	 * Returns the size of this DataPacket.
	 * @return the size of this DataPacket
	 ***************************************/
	public int getPacketSize() {
		return packetSize;
	}
	
	/*************************************************
	 * Returns the file data held in this DataPacket.
	 * @return the file data held in this DataPacket
	 *************************************************/
	public byte[] getPacketData() {
		return packetData;
	}
	
	/***************************************************
	 * Sets the sequence number and file data for this
	 * DataPacket.
	 * @param data data of the packet
	 **************************************************/
	private void setVariables(byte[] data) {
		String tag = new String(data);
		int index1 = tag.indexOf("#");
		int index2 = tag.indexOf("#", index1 + 1);
		String tempSeq = tag.substring(tag.indexOf(":") + 1, tag.indexOf(","));
		seqNum = Integer.parseInt(tempSeq);
		int tagLength = tag.substring(index1, index2 + 1).length();
		packetData = new byte[data.length - tagLength];
		System.arraycopy(data, tagLength, packetData, 0, packetData.length);
		packetSize = packetData.length;
	}
	
	/********************************************************
	 * Returns a string representation of the DatagramPacket
	 * held in this DataPacket.
	 ********************************************************/
	public String toString() {
		return new String(packetData);
	}
}
