package prj4part2;

import java.net.InetAddress;

/*************************************************************************
 * The SlidingWindow class represents a sliding window of DataPackets.
 * It has methods for getting and adding a packet to the window, shifting
 * the window if it receives the ack for the first packet in the window.
 * Also checks if the window is empty meaning its transferred all its
 * packets.
 * @author Kyle Hekhuis
 *************************************************************************/
public class SlidingWindow {

	/** Sliding window of DataPackets. */
	private DataPacket[] slidingWindow;
	/** Length of the sliding window. */
	public int length;
	
	/***************************************************
	 * Creates a sliding window with the passed length.
	 * @param windowLength length of sliding window
	 ***************************************************/
	public SlidingWindow(int windowLength) {
		this.length = windowLength;
		slidingWindow = new DataPacket[length];
	}
	
	/**************************************************************
	 * Adds a packet to the sliding window in the first available
	 * spot in the window.
	 * @param data data to create packet with
	 * @param ip IP of this packet's destination
	 * @param port port number of this packet's destination
	 **************************************************************/
	public void addPacket(byte[] data, InetAddress ip, int port) {
		DataPacket temp = new DataPacket(data, ip, port);
		for (int i = 0; i < slidingWindow.length; i++) {
			if (slidingWindow[i] == null) {
				slidingWindow[i] = temp;
				return;
			}
		}
	}
	
	/*************************************************
	 * Returns the DataPacket at the specified index.
	 * @param index index of DataPacket to get
	 * @return DataPacket at the specified index
	 *************************************************/
	public DataPacket getPacket(int index) {
		return slidingWindow[index];
	}
	
	/************************************************************
	 * Checks if the sliding window is empty, meaning all its
	 * contents have been sent or no packets were ever added
	 * to it.
	 * @return true if sliding window is empty, false otherwise
	 *************************************************************/
	public boolean isEmpty() {
		for (int i = 0; i < slidingWindow.length; i++) {
			if (slidingWindow[i] != null) {
				return false;
			}
		}
		return true;
	}
	
	/*******************************************************
	 * Shifts the sliding windows starting position to the
	 * right by one position.
	 *******************************************************/
	public void shiftWindowRightOne() {
		for (int i = 0; i < slidingWindow.length; i++) {
			if (i == slidingWindow.length - 1) {
				slidingWindow[i] = null;
			} else {
				slidingWindow[i] = slidingWindow[i+1];
			}
		}
	}
	
	/***************************************************************
	 * Used to tell the sliding window an ACK for one of its
	 * packets has been received. Keeps shifting if it has ACKs 
	 * for the new start of the window.
	 * @param ackNum sequence number of packet that was ACKed
	 * @return how many times the sliding window was shifted right
	 ***************************************************************/
	public int gotACK(int ackNum) {
		for (DataPacket packet : slidingWindow) {
			if (packet != null && packet.getSeqNum() == ackNum) {
				packet.setAck(true);
			}
		}
		int count = 0;
		while (checkAcks()) {
			count++;
		}
		return count;
	}
	
	/*****************************************************************
	 * Checks to see if the DataPacket at the start of the sliding
	 * window has been ACKed. Shifts the window right one if it has
	 * @return true if the window was shifted right, false otherwise
	 ******************************************************************/
	private boolean checkAcks() {
		if (slidingWindow[0] != null && slidingWindow[0].getAck()) {
			shiftWindowRightOne();
			return true;
		}
		return false;
	}
}
