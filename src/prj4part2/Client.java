package prj4part2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;

/**********************************************************************
 * The Client class represents a client that request a specified file
 * from a server and saves it to a specified file. Makes sure the 
 * transfer between the client and server is reliable.
 * @author Kyle Hekhuis
 ***********************************************************************/
public class Client {

	/** Socket for the client to listen and receive on. */
	private DatagramSocket clientSocket;
	/** IP of the server to request file from. */
	private InetAddress ipAddress;
	/** Port number of the server to request file from. */
	private int portNum;
	/** Path of where to save the requested file. */
	private String saveFileName;
	/** Contains sequence numbers of received packets. */
	private ArrayList<Integer> receivedPackets;
	/** Checks if the file was successfully request from the server. */
	private boolean fileRequested;
	/** Size of the requested file. */
	private long fileSize;
	/** Checks if the file size was received from the server. */
	private boolean sizeReceived;
	/** Checks if the file was received from the server. */
	private boolean fileReceived;
	
	/********************************************************************
	 * Creates a Client that connects to the passed IP and port number,
	 * request the passed file name, and saves to the passed file name.
	 * @param pIP IP of server to connect to.
	 * @param pPortNum port number of server to connect to
	 * @param fileName file path to request from server
	 * @param saveFileName path of where to save requested file
	 * @throws Exception socket not created
	 *********************************************************************/
	public Client(String pIP, int pPortNum, String fileName, String saveFileName) throws Exception {
		clientSocket = new DatagramSocket();
		clientSocket.setSoTimeout(500);
		ipAddress = InetAddress.getByName(pIP);
		this.portNum = pPortNum;
		this.saveFileName = saveFileName;
		fileRequested = false;
		sizeReceived = false;
		fileReceived = false;
		receivedPackets = new ArrayList<Integer>();
		requestFile(fileName);
		int requestTimeOut = 5;
		while (!fileReceived) {
			if (requestTimeOut == 0) {
				System.out.println("CLIENT: Transfer complete.");
				System.exit(0);
			}
			try {
				
				receivePacket();
			} catch (IOException e) {
				requestTimeOut--;
				continue;
			}
		}
		
	}
	
	/*****************************************************************
	 * Request the passed file from the server and keeps doing so
	 * until the request is acknowledged or the client tries 5 times.
	 * @param fileName file path on server of file to request
	 ******************************************************************/
	private void requestFile(String fileName) {
		int requestTimeOut = 5;
		while (!fileRequested) {
			if (requestTimeOut == 0) {
				System.out.println("CLIENT: Failed to request file. Terminating.");
				System.exit(0);
			}
			String fileRequestString = "#FILE:#" + fileName;
			byte[] sendData = new byte[1016];
			System.arraycopy(fileRequestString.getBytes(), 0, sendData, 0, fileRequestString.getBytes().length);
			sendPacket(sendData);
			System.out.println("CLIENT: Sent request for " + fileName + " from the server.");
			requestTimeOut--;
			
			try {
				receivePacket();
			} catch (IOException e) {
				continue;
			}
		}
	}
	
	/*************************************************
	 * Sends a packet with a checksum attached to it.
	 * @param data packet to send before checksum
	 **************************************************/
	private void sendPacket(byte[] data) {
		byte[] afterChecksum = ChecksumUtil.addChecksum(data);
		DatagramPacket sendPacket = new DatagramPacket(afterChecksum, afterChecksum.length, ipAddress, portNum);
		try {
			clientSocket.send(sendPacket);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*****************************************************
	 * Saves a packet to the specified save file.
	 * @param bytes packet to write to file
	 * @param tagLength length of header on packet
	 * @param off offset of where to save packet to file
	 * @throws Exception if issues with RandomAccessFile
	 *****************************************************/
	private void saveToFile(byte[] bytes, int tagLength, String off) throws Exception {
		RandomAccessFile file = new RandomAccessFile(saveFileName, "rw");
		int offset = Integer.parseInt(off);
		byte[] data = new byte[bytes.length - tagLength];
		System.arraycopy(bytes, tagLength, data, 0, data.length);
		file.seek(offset);
		file.write(data);
		file.setLength(fileSize);
		file.close();
	}
	
	/*************************************************************
	 * Processes a passed packet and handles it according to the
	 * packet's headers.
	 * @param receiveData contents of a packet minus the checksum
	 *************************************************************/
	private void processPacket(byte[] receiveData) {
		String message = new String(receiveData);
		int index1 = message.indexOf("#");
		int index2 = message.indexOf("#", index1 + 1);
		String tag = message.substring(index1, index2 + 1);
		int tagLength = tag.length();
		String identifier = tag.substring(index1 + 1, tag.indexOf(":"));
		//System.out.println("CLIENT: Identifier: " + identifier);
		
		if (identifier.equals("DATA") && sizeReceived) {
			String seqNum = tag.substring(tag.indexOf(":") + 1, tag.indexOf(","));
			String offset = tag.substring(tag.indexOf(",") + 1, index2);
			//System.out.println("Seq Num " + seqNum);
			System.out.println("CLIENT: Received packet with sequence number " + seqNum + ".");
			String ackString = "#DATA ACK:" + seqNum + "#";
			byte[] ack = ackString.getBytes();
			byte[] sendData = new byte[1016];
			System.arraycopy(ack, 0, sendData, 0, ack.length);
			sendPacket(sendData);
			System.out.println("CLIENT: Sent ACK for packet with sequence number " + seqNum + ".");
			if (!receivedPackets.contains(seqNum)) {
				receivedPackets.add(Integer.parseInt(seqNum));
				try {
					saveToFile(receiveData, tagLength, offset);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}
		else if (identifier.equals("SIZE")) {
			System.out.println("CLIENT: Received packet for file size.");
			if (!sizeReceived) {
				sizeReceived = true;
				String size = tag.substring(tag.indexOf(":") + 1, index2);
				System.out.println("CLIENT: Received file size is " + size);
				fileSize = Long.parseLong(size);
				System.out.println("CLIENT: File size is " + fileSize);
			}
			String ackString = "#SIZE ACK:#";
			byte[] ack = ackString.getBytes();
			byte[] sendData = new byte[1016];
			System.arraycopy(ack, 0, sendData, 0, ack.length);
			sendPacket(sendData);
			System.out.println("CLIENT: Sent ACK for file size.");
		}
		else if (identifier.equals("FILE ACK")) {
			fileRequested = true;
			System.out.println("CLIENT: Received ACK for file request.");
		}
	}
	
	/*******************************************************************
	 * Receives a packet and checks if its checksum value is correct.
	 * If it's correct, the packet is processed. Otherwise it's dropped.
	 * @throws IOException exception on receiving a packet
	 ********************************************************************/
	private void receivePacket() throws IOException {
		byte[] receiveData = new byte[1024];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		clientSocket.receive(receivePacket);
		if(ChecksumUtil.comparePacketChecksum(receiveData)) {
			byte[] packetData = ChecksumUtil.getPacketWithoutChecksum(receiveData);
			processPacket(packetData);
		}
	}
	
	/**************************************************************************
	 * Creates a Client object with the user specified server IP, port number,
	 * file request path, and file save path.
	 ***************************************************************************/
	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception {
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("CLIENT: Enter the IP address of the server to connect to: ");
		String ip = inFromUser.readLine();
		System.out.println("CLIENT: Enter the port number of the server to connect to: ");
		int port = Integer.parseInt(inFromUser.readLine());
		System.out.println("CLIENT: Enter the path of the file to download from the server: ");
		String fileName = inFromUser.readLine();
		System.out.println("CLIENT: Enter the path of where to save the file on the client: ");
		String saveFileName = inFromUser.readLine();
		Client c = new Client(ip, port, fileName, saveFileName);
//		Client c = new Client("127.0.0.1", 1994, "src/testFiles/pic1.png", "src/saveFolder/testpic.png");
	}
}
