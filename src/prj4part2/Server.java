package prj4part2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/*************************************************************************
 * The Server class represents a server that listens for a file
 * request from a client, reads the requested file in a specified byte
 * size at a time, sends the bytes in a sliding window, and repeats until
 * the file is fully transferred. Uses UDP sockets and makes the transfer
 * reliable with checksums and acknowledgments.
 * @author Kyle Hekhuis
 **************************************************************************/
public class Server {

	/** Socket for the server to listen and receive on. */
	private DatagramSocket serverSocket;
	/** Current seqNum of bytes read from file. */
	private int seqNum;
	/** Name of file to read data from. */
	private String fileName;
	/** Offset of where to read bytes from the file. */
	private long fileOffset;
	/** How many bytes are left in the file. */
	private long fileSizeRemaining;
	/** Check if the file size has been set. */
	private boolean fileSizeSet;
	/** Size of the file to read. */
	private long fileSize;
	/** Check if the file has been fully read. */
	private boolean fileRead;
	/** Sliding window to send DataPackets. */
	private SlidingWindow sw;
	/** Check if the transfer with the client has been completed. */
	private boolean transferComplete;
	/** Check if the client has received the file size. */
	private boolean sizeReceived;
	/** How big data packets can be before checksum. */
	private static final int PACKET_SIZE_AFTER_CHECKSUM = 1016;
	/** How big data packets can be with checksum. */
	private static final int PACKET_SIZE = 1024;
	/** Check if the server has received the file name from client. */
	private boolean fileNameReceived;
	/** Check if sliding window has been initialized. */
	private boolean windowInitialized;
	
	/***********************************************************
	 * Creates a Server listening on the specified port number.
	 * @param pPortNum port number to listen on
	 * @throws SocketException if serverSocket fails to create
	 **********************************************************/
	public Server(int pPortNum) throws SocketException  {
		serverSocket = new DatagramSocket(pPortNum);
		serverSocket.setSoTimeout(500);
		seqNum = 1;
		fileSizeSet = false;
		fileRead = false;
		fileSizeRemaining = 1;
		transferComplete = false;
		sizeReceived = false;
		fileNameReceived = false;
		windowInitialized = false;
		receivePacketLoop(true);
	}
	
	/*****************************************************
	 * Sends the size of the requested file until an ACK
	 * is received.
	 * @param ip IP of client to send size to
	 * @param port port number of client to send size to
	 ******************************************************/
	private void sendFileSize(InetAddress ip, int port) {
		String temp = "#SIZE:" + fileSize + "#";
		byte[] fileSizeBytes = temp.getBytes();
		while (!sizeReceived) {
			sendPacket(fileSizeBytes, ip, port);
			System.out.println("SERVER: Sent file size packet.");
			try {
				receivePacket();
			} catch (IOException e) {
				continue;
			}
		}
	}
	
	/**************************************************************
	 * Initializes the sliding window with the first 5 packets or
	 * less of the requested file.
	 * @param ip IP of client to send packets to
	 * @param port port number of client to send packets to
	 **************************************************************/
	private void initializeSlidingWindow(InetAddress ip, int port) {
		sw = new SlidingWindow(5);
		try {
			for (int i = 0; i < sw.length; i++) {
				byte[] temp = readFileBytes();
				if (temp != null) {
					sw.addPacket(temp, ip, port);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*******************************************************
	 * Sends the contents of the sliding window, gets ACKs.
	 * Repeats this process until the transfer is complete.
	 *******************************************************/
	private void sendSlidingWindow() {
		while (!transferComplete) {
			for (int i = 0; i < sw.length; i++) {
				DataPacket packet = sw.getPacket(i);
				if (packet != null) {
					sendPacket(packet.getUDPPacket());
					System.out.println("SERVER: Sent data packet with sequence number " +
							packet.getSeqNum() + ".");
				}
			}
			receivePacketLoop(false);
		}
		System.exit(0);
	}
	
	/**************************************************************
	 * Sends a packet containing the passed data plus a checksum.
	 * Sends it to the specified IP and port number.
	 * @param data data to send
	 * @param ip IP of client to send to
	 * @param port port number of client to send to
	 **************************************************************/
	private void sendPacket(byte[] data, InetAddress ip, int port) {
		byte[] afterChecksum = ChecksumUtil.addChecksum(data);
		DatagramPacket sendPacket = new DatagramPacket(afterChecksum, afterChecksum.length, ip, port);
		sendPacket(sendPacket);
	}
	
	/*********************************************************************
	 * Sends a passed DatagramPacket that has a checksum already on it.
	 * @param packet DatagramPacket with checksum to send
	 **********************************************************************/
	private void sendPacket(DatagramPacket packet) {
		try {
			serverSocket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*********************************************************************
	 * Adds packets to the sliding window until the window is full again
	 * or there's no more packets left to add.
	 * @param amount amount of packets to add to the window
	 * @param ip IP of client the packets are destined for
	 * @param port port number of the client the packets are destined for
	 *********************************************************************/
	private void addToSlidingWindow(int amount, InetAddress ip, int port) {
		try {
			for (int i = 0; i < amount; i++) {
				byte[] temp = readFileBytes();
				if (temp != null) {
					sw.addPacket(temp, ip, port);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/****************************************************************************
	 * Processes a passed packet and handles it according to the
	 * packet's headers.
	 * @param packetData contents of a packet minus the checksum
	 * @param clientIP IP of the client this packet was received from
	 * @param clientPort port number of the client this packet was received from
	 *****************************************************************************/
	private void processPacket(byte[] packetData, InetAddress clientIP, int clientPort) {
		String message = new String(packetData);
		int index1 = message.indexOf("#");
		int index2 = message.indexOf("#", index1 + 1);
		String tag = message.substring(index1, index2 + 1);
		String identifier = tag.substring(index1 + 1, tag.indexOf(":"));
		//System.out.println("SERVER: Identifier: " + identifier);
		if (identifier.equals("DATA ACK")) {
			int ackNum = Integer.parseInt(tag.substring(tag.indexOf(":") + 1, index2));
			System.out.println("SERVER: Received ACK for data packet with sequence number " + ackNum + ".");
			addToSlidingWindow(sw.gotACK(ackNum), clientIP, clientPort);
			if (sw.isEmpty()) {
				transferComplete = true;
				System.out.println("Transfer complete.");
			}
		}
		else if (identifier.equals("FILE")) {
			System.out.println("SERVER: Received file request packet.");
			if (!fileNameReceived) {
				fileNameReceived = true;
				fileName = message.substring(index2 + 1).trim();
				System.out.println("server: received file name " + fileName);
			}
			String ackString = "#FILE ACK:#";
	        byte[] ack = ackString.getBytes();
			sendPacket(ack, clientIP, clientPort);
			System.out.println("SERVER: Sent ACK for file request.");
			if (!windowInitialized) {
				initializeSlidingWindow(clientIP, clientPort);
			}
			sendFileSize(clientIP, clientPort);
		}
		else if (identifier.equals("SIZE ACK")) {
			System.out.println("SERVER: Received ACK for file size packet.");
			if (!sizeReceived) {
				sizeReceived = true;
				sendSlidingWindow();
			}
		}
	}
	
	/************************************************************
	 * Runs the receivePacket method in a loop either
	 * continuously or breaks if the socket times out
	 * depending on the passed boolean. True for continue
	 * or false for break on socket timeout.
	 * @param errorSetting boolean setting for loop continuation
	 *************************************************************/
	private void receivePacketLoop(boolean errorSetting) {
		while (true) {
			try {
				receivePacket();
			} catch (IOException e) {
				if (errorSetting) {
					continue;
				} else {
					break;
				}
			}
		}
	}
	
	/*******************************************************************
	 * Receives a packet and checks if its checksum value is correct.
	 * If it's correct, the packet is processed. Otherwise it's dropped.
	 * @throws IOException exception on receiving a packet
	 ********************************************************************/
	private void receivePacket() throws IOException {
		byte[] receiveData = new byte[PACKET_SIZE];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		serverSocket.receive(receivePacket);
		if(ChecksumUtil.comparePacketChecksum(receiveData)) {
			byte[] packetData = ChecksumUtil.getPacketWithoutChecksum(receiveData);
			InetAddress clientIP = receivePacket.getAddress();
			int clientPort = receivePacket.getPort();
			processPacket(packetData, clientIP, clientPort);
		}
	}
	
	/****************************************************************
	 * Reads bytes from the requested file. Keeps track of where
	 * it left off. Adds a checksum to the packet.
	 * @return byte array containing checksum, header, and data for
	 * info from the file
	 * @throws Exception exception occurring from RandomAccessFile
	 ****************************************************************/
	private byte[] readFileBytes() throws Exception {
		if (fileRead) {
			return null;
		}
		RandomAccessFile file = new RandomAccessFile(fileName, "r");
		if (!fileSizeSet) {
			fileSizeRemaining = file.length();
			fileSize = file.length();
			System.out.println("SERVER: File size is: " + fileSize);
			fileSizeSet = true;
			 
		}
		file.seek(fileOffset);
		
		String seqNumTag = "#DATA:" + seqNum + "," + fileOffset + "#";
        byte[] tagBytes = seqNumTag.getBytes();
        int tagSize = tagBytes.length;
        
        int bufferSize = 0;
        if ((fileSizeRemaining / (PACKET_SIZE_AFTER_CHECKSUM - tagSize)) >= 1) {
        	bufferSize = PACKET_SIZE_AFTER_CHECKSUM - tagSize;
        } else {
        	bufferSize = (int) fileSizeRemaining;
        }
        
        byte[] fileBytes = new byte[bufferSize];
		file.read(fileBytes, 0, fileBytes.length);
		file.close();
		
		fileOffset += bufferSize;
		fileSizeRemaining -= bufferSize;
		seqNum++;
		
		if (fileSizeRemaining == 0) {
			fileRead = true;
		}
		byte[] beforeChecksum = ChecksumUtil.combineByteArrays(tagBytes, fileBytes);
		byte[] afterChecksum = ChecksumUtil.addChecksum(beforeChecksum);
		return afterChecksum;
	}
	
	/***********************************************************
	 * Creates a Server object with user specified port number.
	 ************************************************************/
	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception {
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("SERVER: Enter a port number to listen on: ");
		String portNumber = inFromUser.readLine();
		System.out.println("SERVER: Listening for connections on port " + portNumber + ".");
		Server s = new Server(Integer.parseInt(portNumber));
//		Server s = new Server(Integer.parseInt("1994"));
	}
}
