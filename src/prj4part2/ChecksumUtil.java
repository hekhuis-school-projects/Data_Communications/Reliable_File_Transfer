package prj4part2;

import java.nio.ByteBuffer;
import java.util.zip.CRC32;

/******************************************************************************
 * The ChecksumUtil class handles adding checksums to packets, recalculating
 * checksums of packets, getting checksums from packet headers, and comparing
 * two checksums to verify they are the same.
 * @author Kyle Hekhuis
 ******************************************************************************/
public class ChecksumUtil {

	/** Instance of CRC32 to calculate checksums. */
	private static CRC32 crc32 = new CRC32();
	/** Buffer to put the primitive 'long' into for transformations. */
	private static ByteBuffer buffer = ByteBuffer.allocate(8); //Long.BYTES = 8
	
	/***********************************************************************
	 * Gets the checksum info from a passed packet's header and returns it.
	 * @param packet byte array to get checksum from header
	 * @return byte array containing the checksum of the passed packet
	 ***********************************************************************/
	public static byte[] getChecksumFromPacket(byte[] packet) {
		byte[] temp = new byte[8];
		System.arraycopy(packet, 0, temp, 0, temp.length);
		//long checksum = bytesToLong(temp);
		//System.out.println("Checksum from packet: " + checksum);
		return temp;
	}
	
	/*******************************************************************
	 * Calculates the checksum of a passed packet and returns it.
	 * @param packet byte array to find checksum of
	 * @return byte array containing the checksum of the passed packet
	 *******************************************************************/
	public static byte[] findChecksumOfPacket(byte[] packet) {
		byte[] data = new byte[1016];
		System.arraycopy(packet, 8, data, 0, data.length);
		crc32.reset();
		crc32.update(data);
		long checksum = crc32.getValue();
		//System.out.println("Checksum of packet: " + checksum);
		byte[] checksumBytes = longToBytes(checksum);
		return checksumBytes;
	}
	
	/*********************************************************************
	 * Gets the contents of a packet after its checksum has been removed.
	 * @param packet byte array to remove checksum info from
	 * @return byte array without checksum info
	 *********************************************************************/
	public static byte[] getPacketWithoutChecksum(byte[] packet) {
		byte[] temp = new byte[1016];
		System.arraycopy(packet, 8, temp, 0, temp.length);
		//System.out.println("Packet without checksum");
		//System.out.println(new String(temp));
		return temp;
	}
	
	/************************************************************************
	 * Adds a checksum value to the passed byte array.
	 * @param data byte array to add checksum to
	 * @return byte array of passed data with checksum added to front of it
	 ************************************************************************/
	public static byte[] addChecksum(byte[] data) {
		crc32.reset();
		crc32.update(data);
		long checksum = crc32.getValue();
		//System.out.println("Checksum: " + checksum);
		byte[] checksumBytes = longToBytes(checksum);
		return combineByteArrays(checksumBytes, data);
	}
	
	/******************************************************************************
	 * Takes in a packet and gets the checksum from its header. Then recalculates
	 * the checksum of the packet data. Compares the two checksums to see if they
	 * are equal meaning the checksums are valid. Returns true if they are.
	 * @param packet packet to verify checksum on
	 * @return true if checksum is correct
	 ******************************************************************************/
	public static boolean comparePacketChecksum(byte[] packet) {
		byte[] checksumBytes1 = getChecksumFromPacket(packet);
		byte[] checksumBytes2 = findChecksumOfPacket(packet);
		Long checksum1 = bytesToLong(checksumBytes1);
		//System.out.println("Checksum1: " + checksum1);
		Long checksum2 = bytesToLong(checksumBytes2);
		//System.out.println("Checksum2: " + checksum2);
		return checksum1.equals(checksum2);
	}

	/**********************************************************************************************
	 * Takes a long primitive and converts it to a byte array.
	 * @param x long to be converted
	 * @return byte[] representing the passed long
	 * @author Pijusn on Stackoverflow
	 * https://stackoverflow.com/questions/4485128/how-do-i-convert-long-to-byte-and-back-in-java
	 **********************************************************************************************/
	public static byte[] longToBytes(long x) {
        buffer.clear();
		buffer.putLong(0, x);
        return buffer.array();
    }

	/**********************************************************************************************
	 * Takes a byte array and turns it into a long primitive.
	 * @param bytes bytes to convert to a long
	 * @return long created from passed bytes
	 * @author Pijusn on Stackoverflow
	 * https://stackoverflow.com/questions/4485128/how-do-i-convert-long-to-byte-and-back-in-java
	 **********************************************************************************************/
    public static long bytesToLong(byte[] bytes) {
        buffer.clear();
    	buffer.put(bytes, 0, bytes.length);
        buffer.flip();
        return buffer.getLong();
    }
    
    /**********************************************************************
     * Takes two byte arrays and combines them into a single byte array.
     * The first byte array comes before the second byte array in the new
     * combined byte array.
     * @param array1 first byte array
     * @param array2 second byte array
     * @return combined byte array of array1 and array2
     **********************************************************************/
    public static byte[] combineByteArrays(byte[] array1, byte[] array2) {
		int len1 = array1.length;
		int len2 = array2.length;
		byte[] combined = new byte[len1 + len2];
		System.arraycopy(array1, 0, combined, 0, len1);
		System.arraycopy(array2, 0, combined, len1, len2);
		return combined;
	}
}
