Reliable File Transfer
Language: Java
Author: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 457 - Data Communications
Semester / Year: Fall 2016

This program creates a server / client setup where the client request
files from the server. The file transfer between the two is done over
UDP. It uses a sliding window and other features to ensure that
packets are successfully received by and not dropped.
For full specifications see 'CIS 457 Project 2_ Reliable File Transfer over UDP.pdf'.

Usage:
See 'CIS 457 Project 2_ Reliable File Transfer over UDP.pdf'
on how to run and test the program.